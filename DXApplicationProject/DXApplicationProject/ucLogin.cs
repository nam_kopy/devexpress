﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DXApplicationProject
{
    public partial class ucLogin : DevExpress.XtraEditors.XtraUserControl
    {
        public ucLogin()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        public void checkPermission()
        {
            if(txtUserName.Text == "")
            {
                lblErrorUserName.Visible = true;
                this.Visible = true;
            }
        }
    }
}
