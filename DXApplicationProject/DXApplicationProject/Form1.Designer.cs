﻿namespace DXApplicationProject
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.fluentDesignFormContainer = new DevExpress.XtraBars.FluentDesignSystem.FluentDesignFormContainer();
            this.accordionControl1 = new DevExpress.XtraBars.Navigation.AccordionControl();
            this.accordionControlElementDashboard = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElement2 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElementProducts = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElementOrder = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElementReport = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElementAboutUs = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlElementLogout = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.fluentDesignFormControl1 = new DevExpress.XtraBars.FluentDesignSystem.FluentDesignFormControl();
            this.itemNav = new DevExpress.XtraBars.BarStaticItem();
            this.fluentFormDefaultManager1 = new DevExpress.XtraBars.FluentDesignSystem.FluentFormDefaultManager(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.accordionControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fluentDesignFormControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fluentFormDefaultManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // fluentDesignFormContainer
            // 
            this.fluentDesignFormContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fluentDesignFormContainer.Location = new System.Drawing.Point(72, 46);
            this.fluentDesignFormContainer.Margin = new System.Windows.Forms.Padding(6);
            this.fluentDesignFormContainer.Name = "fluentDesignFormContainer";
            this.fluentDesignFormContainer.Size = new System.Drawing.Size(1330, 825);
            this.fluentDesignFormContainer.TabIndex = 0;
            this.fluentDesignFormContainer.Click += new System.EventHandler(this.fluentDesignFormContainer1_Click);
            // 
            // accordionControl1
            // 
            this.accordionControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.accordionControl1.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.accordionControlElementDashboard,
            this.accordionControlElementProducts,
            this.accordionControlElementOrder,
            this.accordionControlElementReport,
            this.accordionControlElementAboutUs,
            this.accordionControlElementLogout});
            this.accordionControl1.Location = new System.Drawing.Point(0, 46);
            this.accordionControl1.Margin = new System.Windows.Forms.Padding(6);
            this.accordionControl1.Name = "accordionControl1";
            this.accordionControl1.OptionsMinimizing.State = DevExpress.XtraBars.Navigation.AccordionControlState.Minimized;
            this.accordionControl1.ScrollBarMode = DevExpress.XtraBars.Navigation.ScrollBarMode.Touch;
            this.accordionControl1.Size = new System.Drawing.Size(72, 825);
            this.accordionControl1.TabIndex = 1;
            this.accordionControl1.ViewType = DevExpress.XtraBars.Navigation.AccordionControlViewType.HamburgerMenu;
            // 
            // accordionControlElementDashboard
            // 
            this.accordionControlElementDashboard.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.accordionControlElement2});
            this.accordionControlElementDashboard.Name = "accordionControlElementDashboard";
            this.accordionControlElementDashboard.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElementDashboard.Text = "Dashboard";
            this.accordionControlElementDashboard.Visible = false;
            this.accordionControlElementDashboard.Click += new System.EventHandler(this.accordionControlElementDashboard_Click);
            // 
            // accordionControlElement2
            // 
            this.accordionControlElement2.Name = "accordionControlElement2";
            this.accordionControlElement2.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement2.Text = "Element2";
            this.accordionControlElement2.Visible = false;
            // 
            // accordionControlElementProducts
            // 
            this.accordionControlElementProducts.Name = "accordionControlElementProducts";
            this.accordionControlElementProducts.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElementProducts.Text = "Products";
            this.accordionControlElementProducts.Visible = false;
            this.accordionControlElementProducts.Click += new System.EventHandler(this.accordionControlElementProducts_Click);
            // 
            // accordionControlElementOrder
            // 
            this.accordionControlElementOrder.Name = "accordionControlElementOrder";
            this.accordionControlElementOrder.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElementOrder.Text = "Order";
            this.accordionControlElementOrder.Visible = false;
            this.accordionControlElementOrder.Click += new System.EventHandler(this.accordionControlElementOrder_Click);
            // 
            // accordionControlElementReport
            // 
            this.accordionControlElementReport.Name = "accordionControlElementReport";
            this.accordionControlElementReport.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElementReport.Text = "Report";
            this.accordionControlElementReport.Visible = false;
            this.accordionControlElementReport.Click += new System.EventHandler(this.accordionControlElementReport_Click);
            // 
            // accordionControlElementAboutUs
            // 
            this.accordionControlElementAboutUs.Name = "accordionControlElementAboutUs";
            this.accordionControlElementAboutUs.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElementAboutUs.Text = "About Us";
            this.accordionControlElementAboutUs.Click += new System.EventHandler(this.accordionControlElementAboutUs_Click);
            // 
            // accordionControlElementLogout
            // 
            this.accordionControlElementLogout.Name = "accordionControlElementLogout";
            this.accordionControlElementLogout.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElementLogout.Text = "Logout";
            this.accordionControlElementLogout.Click += new System.EventHandler(this.accordionControlElementLogout_Click);
            // 
            // fluentDesignFormControl1
            // 
            this.fluentDesignFormControl1.FluentDesignForm = this;
            this.fluentDesignFormControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.itemNav});
            this.fluentDesignFormControl1.Location = new System.Drawing.Point(0, 0);
            this.fluentDesignFormControl1.Manager = this.fluentFormDefaultManager1;
            this.fluentDesignFormControl1.Name = "fluentDesignFormControl1";
            this.fluentDesignFormControl1.Size = new System.Drawing.Size(1402, 46);
            this.fluentDesignFormControl1.TabIndex = 2;
            this.fluentDesignFormControl1.TabStop = false;
            this.fluentDesignFormControl1.TitleItemLinks.Add(this.itemNav);
            // 
            // itemNav
            // 
            this.itemNav.Caption = "???";
            this.itemNav.Id = 1;
            this.itemNav.Name = "itemNav";
            // 
            // fluentFormDefaultManager1
            // 
            this.fluentFormDefaultManager1.Form = this;
            this.fluentFormDefaultManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.itemNav});
            this.fluentFormDefaultManager1.MaxItemId = 2;
            // 
            // frmMain
            // 
            this.Appearance.ForeColor = System.Drawing.Color.Black;
            this.Appearance.Options.UseForeColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1402, 871);
            this.ControlContainer = this.fluentDesignFormContainer;
            this.Controls.Add(this.fluentDesignFormContainer);
            this.Controls.Add(this.accordionControl1);
            this.Controls.Add(this.fluentDesignFormControl1);
            this.FluentDesignFormControl = this.fluentDesignFormControl1;
            this.IconOptions.Image = ((System.Drawing.Image)(resources.GetObject("frmMain.IconOptions.Image")));
            this.Name = "frmMain";
            this.NavigationControl = this.accordionControl1;
            this.Text = " POS System";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.accordionControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fluentDesignFormControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fluentFormDefaultManager1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraBars.FluentDesignSystem.FluentDesignFormContainer fluentDesignFormContainer;
        private DevExpress.XtraBars.Navigation.AccordionControl accordionControl1;
        private DevExpress.XtraBars.FluentDesignSystem.FluentDesignFormControl fluentDesignFormControl1;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElementDashboard;
        private DevExpress.XtraBars.FluentDesignSystem.FluentFormDefaultManager fluentFormDefaultManager1;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement2;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElementProducts;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElementOrder;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElementReport;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElementAboutUs;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElementLogout;
        private DevExpress.XtraBars.BarStaticItem itemNav;
    }
}

