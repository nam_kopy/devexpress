﻿using DevExpress.DXperience.Demos;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Docking2010.Views.WindowsUI;
using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static DevExpress.XtraEditors.RoundedSkinPanel;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace DXApplicationProject
{
    public partial class frmMain : DevExpress.XtraBars.FluentDesignSystem.FluentDesignForm
    {
        public frmMain()
        {
            InitializeComponent();
        }

        async Task LoadModuleAsync(ModuleInfo module)
        {
            await Task.Factory.StartNew(() =>
            {
                if (!fluentDesignFormContainer.Controls.ContainsKey(module.Name))
                {
                    TutorialControlBase control = module.TModule as TutorialControlBase;
                    if (control != null)
                    {
                        control.Dock = DockStyle.Fill;
                        control.CreateWaitDialog();
                        fluentDesignFormContainer.Invoke(new MethodInvoker(delegate ()
                        {
                            fluentDesignFormContainer.Controls.Add(control);
                            control.BringToFront();
                        }
                        ));
                    }
                }
                else
                {
                    var control = fluentDesignFormContainer.Controls.Find(module.Name, true);
                    if (control.Length == 1)
                        fluentDesignFormContainer.Invoke(new MethodInvoker(delegate ()
                        {
                            control[0].BringToFront();
                        }));
                }

            }
            );
        }

        private void fluentDesignFormContainer1_Click(object sender, EventArgs e)
        {
            
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            frmLogin fLogin = new frmLogin();
            fLogin.ShowDialog();
            if(fLogin.Role == "Admin")
            {
                this.itemNav.Caption = $"{accordionControlElementDashboard.Text}";
                this.fluentDesignFormContainer.Controls.Add(new ucDashboard() { Dock = DockStyle.Fill });
                accordionControlElementDashboard.Visible = true;
                accordionControlElementProducts.Visible = true;
                accordionControlElementOrder.Visible = true;
                accordionControlElementReport.Visible = true;
            }
            else if(fLogin.Role == "Seller")
            {
                this.itemNav.Caption = $"{accordionControlElementOrder.Text}";
                this.fluentDesignFormContainer.Controls.Add(new ucOrder() { Dock = DockStyle.Fill });
                accordionControlElementDashboard.Visible = false;
                accordionControlElementProducts.Visible = false;
                accordionControlElementOrder.Visible = true;
                accordionControlElementReport.Visible = false;
            }
            else
            {
                this.itemNav.Caption = $"{accordionControlElementReport.Text}";
                this.fluentDesignFormContainer.Controls.Add(new ucReport() { Dock = DockStyle.Fill });
                accordionControlElementDashboard.Visible = false;
                accordionControlElementProducts.Visible = false;
                accordionControlElementOrder.Visible = false;
                accordionControlElementReport.Visible = true;
            }
        }

        private async void accordionControlElementProducts_Click(object sender, EventArgs e)
        {
            this.itemNav.Caption = $"{accordionControlElementProducts.Text}";
            if (ModulesInfo.GetItem("ucProduct") == null)
                ModulesInfo.Add(new ModuleInfo("ucProduct", "DXApplicationProject.ucProduct"));
            await LoadModuleAsync(ModulesInfo.GetItem("ucProduct"));
        }

        private async void accordionControlElementOrder_Click(object sender, EventArgs e)
        {
            this.itemNav.Caption = $"{accordionControlElementOrder.Text}";
            if (ModulesInfo.GetItem("ucOrder") == null)
                ModulesInfo.Add(new ModuleInfo("ucOrder", "DXApplicationProject.ucOrder"));
            await LoadModuleAsync(ModulesInfo.GetItem("ucOrder"));
        }

        private async void accordionControlElementDashboard_Click(object sender, EventArgs e)
        {
            this.itemNav.Caption = $"{accordionControlElementDashboard.Text}";
            if (ModulesInfo.GetItem("ucDashboard") == null)
                ModulesInfo.Add(new ModuleInfo("ucDashboard", "DXApplicationProject.ucDashboard"));
            await LoadModuleAsync(ModulesInfo.GetItem("ucDashboard"));
        }

        private async void accordionControlElementReport_Click(object sender, EventArgs e)
        {
            this.itemNav.Caption = $"{accordionControlElementReport.Text}";
            if (ModulesInfo.GetItem("ucReport") == null)
                ModulesInfo.Add(new ModuleInfo("ucReport", "DXApplicationProject.ucReport"));
            await LoadModuleAsync(ModulesInfo.GetItem("ucReport"));
        }

        private async void accordionControlElementAboutUs_Click(object sender, EventArgs e)
        {
            this.itemNav.Caption = $"{accordionControlElementAboutUs.Text}";
            if (ModulesInfo.GetItem("ucAboutUs") == null)
                ModulesInfo.Add(new ModuleInfo("ucAboutUs", "DXApplicationProject.ucAboutUs"));
            await LoadModuleAsync(ModulesInfo.GetItem("ucAboutUs"));
        }

        private void accordionControlElementLogout_Click(object sender, EventArgs e)
        {
            frmMain_Load(null, null);
        }
    }
}
