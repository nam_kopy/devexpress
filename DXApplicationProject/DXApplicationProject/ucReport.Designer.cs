﻿namespace DXApplicationProject
{
    partial class ucReport
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnProExcel = new System.Windows.Forms.Button();
            this.btnProPDF = new System.Windows.Forms.Button();
            this.btnOrdPDF = new System.Windows.Forms.Button();
            this.btnOrdExcel = new System.Windows.Forms.Button();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl2
            // 
            this.panelControl2.Appearance.BackColor = System.Drawing.Color.Gold;
            this.panelControl2.Appearance.Options.UseBackColor = true;
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Controls.Add(this.btnOrdPDF);
            this.panelControl2.Controls.Add(this.btnOrdExcel);
            this.panelControl2.Controls.Add(this.label2);
            this.panelControl2.Location = new System.Drawing.Point(28, 238);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(720, 98);
            this.panelControl2.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(30, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(153, 48);
            this.label1.TabIndex = 0;
            this.label1.Text = "Product";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(30, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(179, 72);
            this.label2.TabIndex = 1;
            this.label2.Text = "Order";
            // 
            // btnProExcel
            // 
            this.btnProExcel.BackColor = System.Drawing.Color.SpringGreen;
            this.btnProExcel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnProExcel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnProExcel.ForeColor = System.Drawing.Color.Black;
            this.btnProExcel.Location = new System.Drawing.Point(336, 19);
            this.btnProExcel.Name = "btnProExcel";
            this.btnProExcel.Size = new System.Drawing.Size(156, 64);
            this.btnProExcel.TabIndex = 1;
            this.btnProExcel.Text = "Excel";
            this.btnProExcel.UseVisualStyleBackColor = false;
            // 
            // btnProPDF
            // 
            this.btnProPDF.BackColor = System.Drawing.Color.Red;
            this.btnProPDF.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnProPDF.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnProPDF.ForeColor = System.Drawing.Color.White;
            this.btnProPDF.Location = new System.Drawing.Point(535, 19);
            this.btnProPDF.Name = "btnProPDF";
            this.btnProPDF.Size = new System.Drawing.Size(156, 64);
            this.btnProPDF.TabIndex = 2;
            this.btnProPDF.Text = "PDF";
            this.btnProPDF.UseVisualStyleBackColor = false;
            // 
            // btnOrdPDF
            // 
            this.btnOrdPDF.BackColor = System.Drawing.Color.Red;
            this.btnOrdPDF.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnOrdPDF.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnOrdPDF.ForeColor = System.Drawing.Color.White;
            this.btnOrdPDF.Location = new System.Drawing.Point(535, 21);
            this.btnOrdPDF.Name = "btnOrdPDF";
            this.btnOrdPDF.Size = new System.Drawing.Size(156, 61);
            this.btnOrdPDF.TabIndex = 4;
            this.btnOrdPDF.Text = "PDF";
            this.btnOrdPDF.UseVisualStyleBackColor = false;
            // 
            // btnOrdExcel
            // 
            this.btnOrdExcel.BackColor = System.Drawing.Color.SpringGreen;
            this.btnOrdExcel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnOrdExcel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnOrdExcel.ForeColor = System.Drawing.Color.Black;
            this.btnOrdExcel.Location = new System.Drawing.Point(336, 21);
            this.btnOrdExcel.Name = "btnOrdExcel";
            this.btnOrdExcel.Size = new System.Drawing.Size(156, 61);
            this.btnOrdExcel.TabIndex = 3;
            this.btnOrdExcel.Text = "Excel";
            this.btnOrdExcel.UseVisualStyleBackColor = false;
            // 
            // panelControl1
            // 
            this.panelControl1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.panelControl1.Appearance.Options.UseBackColor = true;
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.btnProPDF);
            this.panelControl1.Controls.Add(this.label1);
            this.panelControl1.Controls.Add(this.btnProExcel);
            this.panelControl1.InvertTouchScroll = true;
            this.panelControl1.Location = new System.Drawing.Point(28, 57);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(720, 98);
            this.panelControl1.TabIndex = 0;
            // 
            // ucReport
            // 
            this.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "ucReport";
            this.Size = new System.Drawing.Size(1491, 751);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnProPDF;
        private System.Windows.Forms.Button btnProExcel;
        private System.Windows.Forms.Button btnOrdPDF;
        private System.Windows.Forms.Button btnOrdExcel;
        private DevExpress.XtraEditors.PanelControl panelControl1;
    }
}
